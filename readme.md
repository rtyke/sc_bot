Sound cloud robot simulates user activity in Sound cloud social network. It visits labels, follow friends and unfollow unmutual friends.

Requirements
--------------
Python 2.7
PostgreSQL


To run the script you need  PostgreSQL:
--------------
    $ brew install postgresql

If there is some problems during installation of PostgreSQL:
--------------
    $ brew update
    $ brew doctor

Create database:
--------------
    $ createdb scf

To run the script you need to install following packages for python:
--------------
- selenium
- psycopg2

I suggest to use virtualvenv (venv) and pip to install necessary packages:
- pip https://pypi.python.org/pypi/pip

- venv http://virtualenv.readthedocs.org/en/latest/virtualenv.html

After installation of pip and virtualvenv clone repo, create folder with
virtulavenv and install necessary packages
--------------
    $ git clone git@gitlab.com:rtyke/sc_bot.git
    $ cd sc_bot
    $ virtualenv --no-site-packages sc_bot_venv
    $ source sc_bot_venv/bin/activate
    $ pip install selenium
    $ pip install psycopg2

Fill in essential_data.py
--------------

Rename essential_data_init.py to essential_data.

Fill in it:

1. db_username  # owner of database *
2. victim_labels  # list of labels, which you want to use for getting new friends.
3. authorizeData  # fill in with data of registered account at soundcloud
4. account  # name of active account in current session
5. how_many_new_friends  # how many new friends you want to follow. If you follow more then 80 users everyday it looks suspicious for soundcloud and may cause ban.
6. unfollow_x_users  # how many unmutual users you want to unfollow and delete likes in case it exists. No limit for this number.
7. unfolow_x_users_dc  # how many users to unfollow (indifferently mutual or unmutual)
8. del_x_likes_dc # how many likes to delete

Run the script to get new friends:

    $ python main.py

Run the script to unfollow unmutual friends:

    $ python unfollow.py

Run the script to unfollow friends (indifferently mutual or unmutual):

    $ python del_all_likes.py

Run the script to delete likes:

    $ python del_all_likes.py

In case if there was unexpected error you need to get information from .log file to json file:

    $ python process_log_file.py'

If you need to add some data in json:

    $ python modify_jsons.py'

Reading json files not very comfortable, to modify it on more readable format use this:

    $ python dict_in_tuples_.py'

* to learn name database owner:

    $ psql scf
    $ \l
    $ \q

'Owner' is your username
