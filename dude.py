# -*- coding: utf-8 -*-
import psycopg2
import random
import util
from selenium.common.exceptions import StaleElementReferenceException

class Dude ():
    def __init__(self, webagent, essential_data, dp, logger=None):
        # self.debug_mode = debug_mode
        self.ed       = essential_data
        if logger is None: self.logger = util.do_nothing  # needed if you dont want to call logger module
        else: self.logger  = logger.c_print
        self.webagent = webagent
        self.dp = dp

        self.users = {'following'             : [],
                      'followers'             : [],
                      'blank'                 : [],
                      'spammer'               : [],
                      'liked_by_me'           : [],
                      'liked_by_me_track_href': [],
                      'deleted_users'         : []}
        self.dont_follow_these_users = []
        self.sc_failure = []
        self.followed_users_counter = 0
        self.abort_script = False

    def open_data_json(self):
        json_dict = self.dp.open_json(template=self.ed.json_name,
                                      position='last')
        if json_dict == {}:
            json_dict = {}
        return json_dict

    def update_db_with_authorizedata(self):
        conn = psycopg2.connect("dbname=scf user=%s" % self.ed.db_username)
        cur = conn.cursor()
        accounts = self.ed.authorizeData.keys()

        try:
            cur.execute('CREATE TABLE counter (account VARCHAR(80), position VARCHAR(80));')
            conn.commit()
        except psycopg2.ProgrammingError:
            conn.rollback()

        for account in accounts:
            cur.execute('SELECT account FROM counter WHERE account=\'%s\';' % account)
            if cur.fetchone() is None:
                cur.execute('INSERT INTO counter (account, position)  VALUES (\'%s\', % s);'
                            % (account, 0))
                conn.commit()

    def preparation(self):
        wa = self.webagent
        folder_name = 'log_' + self.ed.account

        self.update_db_with_authorizedata()
        self.dp.check_folder_existence_create_in_need(folder_name)
        if not self.dp.no_this_extension_in_folder('json', folder_name):
            dicti = {}
            for key in self.ed.dic_keys:
                dicti[key] = []
            dicti['followers'] = wa.open_url_collect_hrefs('https://soundcloud.com/' + self.ed.account + '/followers', xpath_name='users_badge')
            dicti['following'] = wa.open_url_collect_hrefs('https://soundcloud.com/' + self.ed.account + '/following', xpath_name='users_badge')
            dicti['liked_by_me'] = wa.open_url_collect_hrefs('https://soundcloud.com/' + self.ed.account + '/likes', xpath_name='artist_name_in_pls')
            dicti['liked_by_me_track_href'] = wa.get_visible_hrefs('track_name_in_pls')
            self.dp.serialize_array(array=dicti,
                                    file_name=util.today_and_cur_t() + '_' + self.ed.json_name)



    def get_all_tracks_responses(self, tracks_hrefs_list):
        """
        Собирает данные об отзывах на треки из tracks_hrefs_list. Данные при
        каждом запуске скрипта перезаписываются без учета предыдущих данных.
        Создает словарь вида {track1: {type_of_response1 : [user1, user2,...],
                                       type_of_response2 : [user1, user2,...],
                                       ..........},
                              track2: {type_of_response1 : [user1, user2,...],
                                       type_of_response2 : [user1, user2,...],
                                       ........},
                              ........,
                              }
        """
        wa = self.webagent
        tracks_response = {}
        for el in tracks_hrefs_list:
            track_name = el.split('/')[-1]
            tracks_response[track_name] = {}
            tracks_response[track_name]['likes'] = \
                wa.get_hrefs_from_page_in_array(el + '/likes', 'users_badge')
            tracks_response[track_name]['reposts'] = \
                wa.get_hrefs_from_page_in_array(el + '/reposts', 'users_badge')
            wa.open_url(el + '/comments')
            if wa.check_existence('no_comments_on_track'):
                tracks_response[track_name]['comments'] = []
            else:
                wa.scroll_page_to_the_end('user_name_in_comments')
                tracks_response[track_name]['comments'] = wa.get_visible_hrefs('user_name_in_comments')
            wa.open_url(el + '/sets')
            if wa.check_existence('no_sets_for_track'):
                tracks_response[track_name]['sets'] = []
            else:
                wa.scroll_page_to_the_end('artist_name_in_pls')
                tracks_response[track_name]['sets'] = wa.get_visible_hrefs('artist_name_in_pls')
        return tracks_response

    def users_to_follow(self, new_users, dont_follow):
        wa = self.webagent
        LGM = self.logger

        users_hrefs = util.get_hrefs_from(new_users)
        following_tuples = \
            self.__pick_significant_followers(users_hrefs,
                                              self.ed.min_followers,
                                              self.ed.max_followers)
        LGM('len significant_followers : %s' % len(following_tuples))
        following_tuples = filter(lambda x: x[1] not in dont_follow,
                                  following_tuples)

        following = []
        for el in following_tuples:
            following.append(el[1])
        return following

    def click_vanishing_elements_on_page(self, url,
                                         el_xpath_to_click,
                                         el_xpath_to_check_identity,
                                         code_for_successfull_click,
                                         how_many,
                                         wait_for='presence'):
        """Click all elements on page. After successful click element have to
        vanish. If it doesn't vanish, then click was no successful"""
        def try_get_identity_href():
            try:
                first_el_to_check_identity = wa.get_href_from_el(el_xpath_to_check_identity, waiting_time=10)
            except AttributeError:
                first_el_to_check_identity = None
            return first_el_to_check_identity

        wa = self.webagent
        LGM = self.logger
        counter = how_many

        array = []
        wa.open_url(url)
        LGM('Open %s' % url)
        LGM('We are going to click \'%s\' %s times' % (el_xpath_to_click, how_many))
        first_el_to_check_identity = try_get_identity_href()
        while counter > 0:
            if first_el_to_check_identity is None:
                LGM('No more elements \'%s\' on page' % el_xpath_to_check_identity)
                break
            counter -= 1
            wa.get_element_by_xpath_name(el_xpath_to_click, wait_for=wait_for).click()
            first_el_to_check_identity_after_click = try_get_identity_href()
            while first_el_to_check_identity_after_click == first_el_to_check_identity:
                wa.sleep(timeout=1, countdown=True)
                first_el_to_check_identity_after_click = try_get_identity_href()
            LGM('st:%s %s was clicked, %s left to click' %
                (code_for_successfull_click, first_el_to_check_identity, counter))
            array.append(first_el_to_check_identity)
            first_el_to_check_identity = first_el_to_check_identity_after_click
        return array


    def like_and_play_random_track(self, user_url=None):
        LGM = self.logger
        wa = self.webagent
        u = util
        if user_url:
            self.webagent.open_url(user_url)
        else:
            user_url = wa.get_current_url()
        artists_list = wa.get_elements_by_xpath_name('artist_name_in_pls')
        artist_hrefs = util.get_hrefs_from(artists_list)
        play_buttons = wa.get_elements_by_xpath_name('play_button')
        like_buttons = wa.get_elements_by_xpath_name('like_button')
        tracks_names = wa.get_elements_by_xpath_name('track_name_in_pls')
        array_for_random_play = u.zip_arrays_with_condition(lambda x: x[0] == user_url,
                                                            artist_hrefs, play_buttons,
                                                            like_buttons, tracks_names)
        if len(array_for_random_play) >= 3:
            end = 2
        else:
            end = len(array_for_random_play) - 1
        random_num = random.randint(0, end)
        random_track = array_for_random_play[random_num]
        random_time = random.randint(self.ed.a, self.ed.b)
        wa.click_obj(random_track[1])  # click play
        track_href = random_track[3].get_attribute('href')
        track_name = random_track[3].text
        LGM('We will listen track \'%s\' for %s seconds. We will also like it '
            'and follow this guy' % (track_name, random_time))
        LGM('st:alwtf %s like of this track was done' % track_href)
        self.users['liked_by_me_track_href'].append(track_href)
        self.users['liked_by_me'].append(user_url)
        LGM('st:als %s Track was liked successfully' % user_url)
        wa.sleep(random_time)
        wa.click_obj(random_track[2])  # click like


    def is_our_client(self, user_url=None):
        LGM = self.logger
        wa = self.webagent

        our_client = False
        if user_url:
            wa.open_url(user_url)
        else:
            user_url = wa.get_current_url()

        if not wa.get_element_by_xpath_name('follow_button'):
            LGM('Cant find follow button on a page')
            if wa.check_existence('no_user'):
                self.users['deleted_users'].append(user_url)
                LGM('Cant find this user')
            else:
                LGM('Cant find follow_button because some obscure reason')
                self.abort_script = True
                return False
            self.sc_failure.append(user_url)
            self.dont_follow_these_users.append(user_url)  # !
        elif wa.user_is_followed_by_me():
            LGM('st:iafu %s I already follow this user' % user_url)
            self.users['following'].append(user_url)
        elif self.user_is_my_follower():
            LGM('st:uimf %s this user follows me' % user_url)
            self.users['followers'].append(user_url)
            self.dont_follow_these_users.append(user_url)  # !
        elif self.user_is_blank() is None:
            self.sc_failure.append(user_url)
            self.users['blank'].append(user_url)
            self.dont_follow_these_users.append(user_url)  # !
            LGM('Looks like there is no tracks on a page, but there is no'
                'message about it. Please check this user after work will'
                'be completed. If user doesn\'t have blank page delete'
                'it from lists if blank users')
        elif self.user_is_blank() is True:
            LGM('st:bp %s blank_page' % user_url)
            self.users['blank'].append(user_url)
            self.dont_follow_these_users.append(user_url)  # !
        # elif user_is_spammer(): TODO проверка на спаммеров
        else:
            wa.scroll_page('artist_name_in_pls')
            if self.user_was_ever_liked_by_me():
                LGM('st:als %s I already liked this user' % user_url)
                self.users['liked_by_me'].append(user_url)
                self.dont_follow_these_users.append(user_url)   # !
            else:
                our_client = True
                LGM('%s is our client' % user_url)
        return our_client

    def play_like_follow(self, user_url):
        LGM = self.logger
        wa = self.webagent

        following_done = False
        wa.open_url(user_url)
        if not self.is_our_client():
            return False
        else:
            # TODO  сейчас пользователь определяется как респостер, если у него
            # TODO на стр. ничего нет, и нет при этом надписи 'user hasnt shared
            # TODO any public records'. Хорошо бы таких людей вообще не френдить
            if self.user_is_reposter():
                LGM('reposter')
                wa.sleep(wa.timeout)
                wa.click_follow()  # click follow
                self.users['following'].append(user_url)
                self.followed_users_counter += 1
                LGM('st:fs %s User was followed successfully' % user_url)
                LGM('%s new friends' % self.followed_users_counter)
            else:
                LGM('not reposter and our client')
                self.like_and_play_random_track()
                wa.click_follow()  # click follow
                LGM('st:fs %s User was followed successfully' % user_url)
                self.users['following'].append(user_url)
                self.followed_users_counter += 1
                LGM('%s новых друзей \n' % self.followed_users_counter)
        return following_done


    def follow_users(self, max_users_to_follow):
        LGM = self.logger
        wa = self.webagent

        following_was_successful = False
        scrolls_amount = 0
        already_followed = []
        while len(already_followed) < self.ed.freshman_x and self.followed_users_counter < max_users_to_follow:
            if scrolls_amount == 0:
                preceding_users = []
            else:
                preceding_users = wa.get_elements_by_xpath_name('users_badge')
            LGM('Begin scrolling, %s scrolls need' % scrolls_amount)
            if not wa.scroll_x_times(scrolls_amount, 'users_badge'):
                LGM('It looks like too many time it takes too many time for '
                    'scrolling. Stop process this label')
                break
            LGM('End of scrolling')
            posterior_users = wa.get_elements_by_xpath_name('users_badge')
            new_users = util.array_diff(posterior_users, preceding_users)
            relevant_users = self.users_to_follow(new_users,
                                                  self.dont_follow_these_users)
            for relevant_user in relevant_users:
                if self.followed_users_counter >= max_users_to_follow:
                    break
                elif len(already_followed) >= self.ed.freshman_x:
                    LGM('Looks like there is no more fresh users on this label')
                    break
                elif relevant_user in self.users['following']:
                    already_followed.append(relevant_user)
                elif self.play_like_follow(relevant_user):
                    self.followed_users_counter += 1
                    following_was_successful = True
                elif self.abort_script:
                    return False
            scrolls_amount += 1
        return following_was_successful

    def label_rotation(self, max_users_to_follow):
        LGM = self.logger
        wa = self.webagent
        conn = psycopg2.connect("dbname=scf user=%s" % self.ed.db_username)
        cur = conn.cursor()
        cur.execute('SELECT position FROM counter WHERE account = \'%s\'' % self.ed.account)
        stopped_at_position = int(cur.fetchone()[0])
        victim_labels = util.shift_elements_in_array(self.ed.victim_labels, stopped_at_position)

        full_label_iteration = 0
        futile_labels = []
        LGM('We are going to follow %s users' % max_users_to_follow)
        while self.followed_users_counter < max_users_to_follow:
            if self.abort_script:
                break
            LGM('%s label rotations was done' % full_label_iteration)
            if len(futile_labels) == self.ed.label_capacity_x:
                LGM('Label capacity is not enough, %s labels one after another'
                    'was futile: %s. Please provide new labels' % (self.ed.label_capacity_x, futile_labels))
                break
            for victim_label in victim_labels:
                if self.followed_users_counter >= max_users_to_follow:
                    break
                if len(futile_labels) == self.ed.label_capacity_x:
                    break
                LGM('Victim label is %s' % victim_label)
                wa.open_url('https://soundcloud.com/' + victim_label + '/followers')
                if not self.follow_users(max_users_to_follow) and self.abort_script:
                    break
                elif not self.follow_users(max_users_to_follow):
                    futile_labels.append(victim_label)
                    print victim_labels
                else:
                    del futile_labels[:]
                stopped_at_position = (stopped_at_position + 1) % len(self.ed.victim_labels)
                cur.execute('UPDATE counter SET position=%s WHERE account = \'%s\''
                            % (stopped_at_position, self.ed.account))
                conn.commit()
            full_label_iteration += 1
        cur.close()
        conn.close()



    def __pick_significant_followers(self, array, min_followers,
                                     max_followers):
        LGM = self.logger
        length_to_check = len(array)
        followers_numbers =\
            self.__get_list_of_follower_numbers_on_avatars(length_to_check)
        x_in_range = lambda x: x[0] in range(min_followers, max_followers + 1)
        significant_followers = util.zip_arrays_with_condition(x_in_range,
                                                                 followers_numbers,
                                                                 array)
        return significant_followers

    def __get_list_of_follower_numbers_on_avatars(self, length_to_check):
        """ xpath 'text_on_avatar' должен находить элемент, внутри которого
        еще два элемента. Метод .text сможет извлечь из этого элемента следующее:
        '17 followers\n17'. Для пользователя без followers el.text == ''.
        Следовательно для определния пользователей без followers используем
        условие - если в el.text не найден '\n' (перенос строки), то у этого
        пользователя нет followers."""
        data_on_all_badges =\
            self.webagent.get_elements_by_xpath_name('text_on_avatar')
        start = len(data_on_all_badges) - length_to_check
        end = len(data_on_all_badges) + 1
        data_on_current_badges = data_on_all_badges[start:end]
        num_fol = []
        for d in data_on_current_badges:
            if d.text.find('\n') != -1:
                num = d.text.split('\n')[-1]
                # TODO process 14.5K number of followers on badge properly
                if num[-1:] == 'K':
                    num = '6000'
                num_fol.append(util.string_to_num(num))
            else:
                num_fol.append(0)
        return num_fol

    def get_followers(self, user_url):
        """collect all users followers urls"""
        # TODO удалить этап создания followers_array, просто возвращать hrefs_my_followers
        wa = self.webagent
        followers_array = []

        wa.open_url(user_url + '/followers')
        wa.scroll_page_to_the_end('users_badge')
        my_followers_list = wa.get_elements_by_xpath_name('users_badge')
        hrefs_my_followers = util.get_hrefs_from(my_followers_list)
        for h in hrefs_my_followers:
            followers_array.append(h)
        return followers_array

    def get_tracks_responses_for_user(self, user_url):
        tracks_hrefs_list = self.get_tracks_hrefs_from_page(user_url)
        if not tracks_hrefs_list:
            self.webagent.abort("user has no tracks")
        responses_dict = self.get_all_tracks_responses(tracks_hrefs_list)
        if not responses_dict:
            self.webagent.abort("user has no responses")
        return responses_dict

    def get_tracks_hrefs_from_page(self, page_url):
        wa = self.webagent

        wa.open_url(page_url)
        wa.scroll_page_to_the_end('track_name_in_pls')
        tracks_names_list = wa.get_elements_by_xpath_name('track_name_in_pls')
        artists_names_list = wa.get_elements_by_xpath_name('artist_name_in_pls')

        tracks_names_hrefs = util.get_hrefs_from(tracks_names_list)
        artists_hrefs = util.get_hrefs_from(artists_names_list)

        my_tracks_array = util.zip_arrays_with_condition(
            lambda x: x[0] == wa.browser.current_url,
            artists_hrefs,
            tracks_names_hrefs)
        my_tracks = []
        for el in my_tracks_array:
            my_tracks.append(el[1])
        return my_tracks

    def user_was_ever_liked_by_me(self):
        was_ever_liked = False
        done = False
        while not done:
            likes = self.webagent.get_elements_by_xpath_name('like_button')
            like_text = []
            for like in likes:
                try:
                    like_text.append(like.get_attribute('title'))
                except StaleElementReferenceException:
                    break
            done = True
            for text in like_text:
                if text in("Liked", "Liked Playlist"):
                    was_ever_liked = True
                    break
                else:
                    was_ever_liked = False
        return was_ever_liked

    def user_is_reposter(self):
        is_reposter = False
        artists = self.webagent.get_elements_by_xpath_name(
            'artist_name_in_pls')
        if not artists: self.webagent.abort('no artists on page!')
        artists2 = []
        for a in artists:
            if a.get_attribute('href') == self.webagent.get_current_url():
                artists2.append(a.get_attribute('href'))
        if len(artists2) == 0:
            is_reposter = True
        return is_reposter

    def user_is_my_follower(self):
        button =\
            self.webagent.get_element_by_xpath_name('follow_button')
        follower = False
        if button.text == 'Follow back':
            follower = True
        return follower

    def user_is_blank(self):
        is_blank = False
        artists_on_page = self.webagent.get_elements_by_xpath_name('artist_name_in_pls')
        if not artists_on_page:
            if self.webagent.check_existence('blank_user'):
                is_blank = True
            else:
                is_blank = None  # there page is blank but there is no message about it on users page
        return is_blank

    def user_is_spammer(self):
        sc = self.ed.spammer_check
        is_spammer = False
        if self.get_number_of_following() > sc[0]:
            is_spammer = True
        elif self.get_number_of_following() < sc[1] \
                and self.get_number_of_followers() > sc[2]:
            is_spammer = True
        return is_spammer

    def get_number_of_following(self):
        """define number of following (data from users page)"""
        wa = self.webagent
        # string '100 following'
        number_of_following = wa.get_element_by_xpath_name('number_of_following')
        el = number_of_following
        # number (100) from string '100 following'
        number_of_following = util.string_to_num(el.text[:el.text.find(' ')])
        return number_of_following

    def get_number_of_followers(self):
        """define number of followers (data from users page)"""
        wa = self.webagent
        # string '100'
        number_of_followers = wa.get_element_by_xpath_name('number_of_followers')
        el = number_of_followers
        # number (100) from string '100'
        number_of_followers = util.string_to_num(el.text)
        return number_of_followers

    def skip_list(self, track_response_dic, json_dic, *keys):
        """First and second arguments - from which dictionary we are going to
        create skip_list. The third argument which keys we are going to use in
        second dictionary"""
        skip_list = []
        # put all users which gave any response on my tracks in one list
        part1 = util.get_all_values_from_dics_of_dics(track_response_dic)
        # put certain users from json_dict in one list
        part2 = util.get_values_by_keys(json_dic, *keys)
        skip_list += part1 + part2
        skip_list = util.del_duplicates_in_array(skip_list)
        return skip_list

    def skip_list_for_unfollow(self, track_response_dic, json_dic):
        """don't unfollow our followers and and users which gave us any
        response"""
        return self.skip_list(track_response_dic, json_dic, 'followers')

    def set_previous_following(self, json_dic):
        self.users['following'] = json_dic['following'][:]
        print self.users['following']

    def skip_list_for_following(self, track_response_dic, json_dic):
        """don't follow all users from json_dict and users which gave us any
         response"""
        return self.skip_list(track_response_dic,
                              json_dic,
                              *('blank', 'spammer', 'followers', 'liked_by_me',
                                'ex_followers', 'ex_following', 'deleted_likes',
                                'deleted_track')
                              )

    def get_likes_of_authors_tracks(self, url=None):
        wa = self.webagent
        if url:
            wa.open_url(url)
            user_url = url
        else:
            user_url = wa.get_current_url()
        artist_hrefs = wa.get_hrefs_from_elements('artist_name_in_pls')
        like_buttons = wa.get_elements_by_xpath_name('like_button')
        likes_of_authors_tracks = [x[1] for x in zip(artist_hrefs, like_buttons)
                                   if x[0] == user_url]
        return likes_of_authors_tracks

    def unfollow_unlike(self, data_dic, skip_list, unfollow_x_users, unlike=True):
        wa = self.webagent
        LGM = self.logger

        counter = 0
        unfollowing_array = []
        deleted_likes = []
        deleted_users = []
        deleted_tracks = []
        sc_failure = []
        critical_error = False
        LGM('We are going to unfollow %s users' % unfollow_x_users)
        for el in [x for x in data_dic['following'] if x not in skip_list]:
            if counter == unfollow_x_users:
                LGM('Stop, enough users was unfollowed: %s' % str(unfollow_x_users))
                break
            LGM('\n')
            LGM('Open %s' % el)
            wa.open_url(el)
            LGM('Try unfollow: %s' % el)
            fbm = wa.user_is_followed_by_me()
            # print fbm
            if fbm is None:
                # TODO это дублирование работы метода get_element_by_xpath_name в wa
                # TODO возвращать через него только сам элемент или None, причину
                # TODO возвращения None определять уже здесь
                if wa.check_existence('no_user'):
                    LGM('st:du %s This user has deleted it\'s account.' % el)
                    deleted_users.append(el)
                elif wa.check_smth_went_wrong():
                    LGM('st:ufscf %s Error smth_went_wrong. Too many reloads' % el)
                    sc_failure.append(el)
                else:
                    LGM('Some obscure reason why we can\'t find for follow button'
                        'Script was stopped, all data will be storaged')
                    util.s_critical_error()
                    critical_error = True
                    break
            elif not fbm:  # if wa.user_is_followed_by_me() is False
                # TODO не уверена что разумно прекращать из-за этого работу скрипта
                # TODO т.к. практика показала что пользователи могут меняться именами
                # TODO (редко, но тем не менее)
                # TODO возможно будет правильнее вести список с такими юзерами
                # TODO и по окончанию работы скрипта удалять их из following и liked_by_me
                LGM('st: idfu % s Smth went wrong! i dont follow this user' % el)
                util.s_critical_error()
                critical_error = True
                break
            else:
                # button = wa.get_element_by_xpath_name('follow_button')
                unfollow = wa.click_unfollow()  # click unfollow
                if unfollow:
                    unfollowing_array.append(el)
                    counter += 1
                    LGM('st:us %s Unfollowed successful' % el)
                    LGM('Remained: %s' % (unfollow_x_users - counter))
                else:
                    LGM('st:ufscf %s Can\'t unfollow this user. '
                        'Looks like sc failure. Check it!' % el)
                    sc_failure.append(el)
                if unlike and el in data_dic['liked_by_me']:
                    LGM('Try del like: %s' % el)
                    del_done = self.del_like_on_users_page()
                    # if not del_done:
                    if del_done == 'too_many_likes':
                        LGM('!!!!! ATTENTION!Too many likes on one page %s.\n Stop script' % el)
                        critical_error = True
                        break
                    elif del_done == 'no_likes':
                        LGM('st:ld %s .Can\'t find likes on page.' % el)
                        deleted_tracks.append(el)
                        util.s_critical_error()
                        # critical_error = True
                        # break
                    elif del_done == 'sc_failure':
                        LGM('st:lfscf %s Can\'t del like of this user. '
                            'Looks like sc failure. Check it!' % el)
                        sc_failure.append(el)
                        util.s_critical_error()
                    else:
                        deleted_likes.append(el)
                        LGM('st:ls %s Del like was done!' % el)
        result = {'unfollowing' : unfollowing_array,
                  'deleted_likes' : deleted_likes,
                  'deleted_users': deleted_users,
                  'critical_error' : critical_error,
                  'deleted_tracks' : deleted_tracks}
        if sc_failure:
            result['sc_failure'] = sc_failure
        LGM('%s users was unfollowed' % str(counter))
        return result


    def del_like_on_users_page(self, url=None):
        # TODO подумать о том чтобы удалять лайки не при заходе на страницу пользователя, а заходя
        # TODO на страницу конкретного лайка. Для этого нужно будет предварительно обновить все
        # TODO инфморацию о лайках (засечь сколько времени).
        # TODO pros: 1. сразу видно кто удалил свои треки 2. не надо тратить время на скролл страницы
        # TODO cons: подумать....
        wa = self.webagent
        LGM = self.logger

        def unlike_loop(scrl_counter):
            likes = wa.get_elements_by_xpath_name('like_button')
            new = 0
            last_original_likes, liked_tracks = get_liked_tracks(likes, new)
            if len(liked_tracks) > 1:
                unlike_done = False
                return liked_tracks, unlike_done
            elif len(liked_tracks) == 0:
                len_before = len(wa.get_elements_by_xpath_name('like_button'))
                scrl = wa.scroll_page('artist_name_in_pls')
                len_after = len(wa.get_elements_by_xpath_name('like_button'))
                new = len_after - len_before
                print 'new upload', new
                if not scrl:
                    unlike_done = False
                    # TODO может сбоить на этапе скролла долго загружается информация
                    # TODO как бороться пока не поняла
                    # TODO пример: https://soundcloud.com/clusterbooking, 13.02 в
                    # TODO 2015.06.24_13.43.48_data.mov
                    LGM('st:ld %s I\'ve scrolled page till the end  (%s times). '
                        'Can\'t find likes on it!' % (user_url, scrl_counter))
                    return [], unlike_done
                else:
                    return unlike_loop(scrl_counter + 1)
            elif len(liked_tracks) == 1:
                liked_track = liked_tracks[0]
                unlike_done = wa.click_unlike(liked_track, last_original_likes)
                if not unlike_done:
                    LGM('%s Wasn\'t deleted because of souncloud failure. '
                        'Check it later.'
                        % wa.get_tracks_titles_href_by(liked_track))
                return liked_track, unlike_done

        def get_liked_tracks(likes, new):
            # получить кол-во новых загруженных треков
            last_likes = likes[-new:]
            last_artist_hrefs = \
                wa.get_hrefs_from_elements('artist_name_in_pls')[-new:]
            last_original_likes = [x[1] for x in zip(last_artist_hrefs, last_likes)
                                   if x[0] == user_url]
            liked_tracks = [like for like in last_original_likes
                            if like.text in ("Liked", "Liked Playlist")]
            return last_original_likes, liked_tracks

        if url:
            wa.open_url(url)
            user_url = url
        else:
            user_url = wa.get_current_url()
        amount = wa.number_of_elements_on_a_page('like_button')
        if amount > 0:
            liked_track, unlike_done = unlike_loop(scrl_counter=0)
            if type(liked_track) is list and len(liked_track) > 1:
                del_done = 'too_many_likes'
            elif liked_track and not unlike_done:
                del_done = 'sc_failure'
            elif not liked_track and not unlike_done:
                # TODO встроить проверку на разлогин
                del_done = 'no_likes'
            else:
                LGM('%s like of this track was successful deleted!' % wa.get_tracks_titles_href_by(liked_track))
                del_done = 'done'
        else:
            LGM('There is no *any* tracks on this page!')
            del_done = 'no_likes'
        return del_done

