# -*- coding: utf-8 -*-
import logger as logger_module
import webagent
import datapersistence
import dude as dude_module
import util
import essential_data

if __name__ == '__main__':
    ed = essential_data
    u = util
    user_url = 'https://soundcloud.com/' + ed.account
    log_folder = 'log_' + ed.account
    logger = logger_module.Logger(folder=log_folder,
                                  file_name=ed.logging_file)
    dp = datapersistence.DataPersistence(folder=log_folder,
                                         test_mode=ed.test_mode)
    wa = webagent.WebAgent(captcha=False,
                           logger=logger,
                           test_mode=ed.test_mode,
                           check_alert=ed.check_alert)
    dude = dude_module.Dude(webagent=wa,
                            essential_data=ed,
                            dp=dp,
                            logger=logger)

    LGM = logger.c_print
    dude.preparation()
    if not ed.test_mode:
        wa.login()
    else:
        print 'WE ARE WORKING IN TESTING MODE'
        wa.open_url(user_url)
    json_dict = dude.open_data_json()
    if ed.update_inf_before_follow:
        tracks_response_dic = dude.get_tracks_responses_for_user(user_url)
        if len(tracks_response_dic) == 0:
            wa.abort("Error via collecting data about track responses. No responses")
    else:
        tracks_response_dic = dp.open_json('tracks_response_dic', 'last')

    dude.skip_list_for_following(tracks_response_dic, json_dict)
    dude.set_previous_following(json_dict)

    dude.label_rotation(ed.how_many_new_friends)
    if dude.sc_failure:
        LGM('There was soundcloud failures during attempt to follow this users '
            '\n%s\n. Please check log to get detailed information'
            % dude.sc_failure)
    if dude.abort_script:
            LGM('Script was aborted, because of some obscure error. All data'
                'would be serialized')

    print '**********'
    json_dict = util.merge_dics(json_dict, dude.users)

    dp.serialize_array(array=json_dict,
                       file_name=u.today_and_cur_t() + '_' + ed.json_name)

    dp.serialize_array(array=tracks_response_dic,
                       file_name=u.today_and_cur_t() + '_tracks_response_dic')








