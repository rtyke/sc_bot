# -*- coding: utf-8 -*-
import logger as logger_module
import webagent
import datapersistence
import dude as dude_module
import util
import essential_data

if __name__ == '__main__':
    ed = essential_data
    u = util
    user_url = 'https://soundcloud.com/' + ed.account
    log_folder = 'log_' + ed.account
    if ed.test_mode:
        log_folder += '_test'
    logger = logger_module.Logger(folder=log_folder,
                                  file_name=ed.logging_file)
    LGM = logger.c_print
    dp = datapersistence.DataPersistence(folder=log_folder,
                                         test_mode=ed.test_mode)
    wa = webagent.WebAgent(captcha=False,
                           logger=logger,
                           test_mode=ed.test_mode,
                           check_alert=ed.check_alert)
    dude = dude_module.Dude(webagent=wa,
                            essential_data=ed,
                            dp=dp,
                            logger=logger)


    how_many = ed.del_x_likes_dc
    if not ed.test_mode:
        wa.login()
    else:
        print 'WE ARE WORKING IN TESTING MODE'
        wa.open_url(user_url)
    json_dict = dude.open_data_json()
    deleted_likes = dude.click_vanishing_elements_on_page(url=user_url + '/likes',
                                                          el_xpath_to_click='like_button',
                                                          el_xpath_to_check_identity='artist_name_in_pls',
                                                          code_for_successfull_click='ls',
                                                          how_many=how_many,
                                                          wait_for='clickable')

    json_dict['deleted_likes'] = json_dict['deleted_likes'] + deleted_likes
    json_dict['liked_by_me'] = util.array_diff(json_dict['liked_by_me'], deleted_likes)
    LGM('Likes for %s was deleted' % ed.account)

    # ex_following = dude.click_vanishing_elements_on_page(url=user_url + '/following',
    #                                                      el_xpath_to_click='follow_button_on_user_badge',
    #                                                      el_xpath_to_check_identity='users_badge',
    #                                                      code_for_successfull_click='us',
    #                                                      how_many=210)
    #
    # json_dict['ex_following'] = json_dict['ex_following'] + ex_following
    # json_dict['following'] = util.array_diff(json_dict['following'], ex_following)
    # LGM('Followings for %s was deleted' % ed.account)

    dp.serialize_array(array=json_dict,
                       file_name=u.today_and_cur_t() + '_' + ed.json_name)











