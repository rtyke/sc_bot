# -*- coding: utf-8 -*-
import json
import time
import datetime
from copy import deepcopy
from datetime import date
from itertools import chain
from sys import exit
import subprocess
import os


# SOUNDS
def play_sound(name):
    path = os.getcwd()
    return subprocess.call(["afplay", path + '/sounds/' + name + '.wav'])

def s_captcha():
    play_sound("cap")

def s_critical_error():
    play_sound("ce")

def s_finish():
    play_sound("tada")

def s_bad_finish():
    play_sound("bad_finish")

def s_abort():
    play_sound("abort")


# DICTIONARIES MANIPULATIONS

def get_values_by_keys(dic, *keys):
    """Put all values of certain keys if dict in list.
    To get values from all keys use:
    get_values_by_keys(dic, *(dic.keys())"""
    # TODO check what is faster for dic with list as values:
    # TODO get_values_by_keys(dic, *(dic.keys()) or
    # TODO flatten_list(dic.values())
    values = []
    for key in keys:
        values += dic.get(key, [])
    return values


def get_unique_values(dic):
    """Get all unique values from dict and put it to the flat list"""
    values = get_values_by_keys(dic, *(dic.keys()))
    unique_values = del_duplicates_in_array(values)
    return unique_values


def get_all_values_from_dics_of_dics(dic):
    """Put all values from dict like:
    {dic1: {inner_dic1_1: [..., ..., ...],
            inner_dic1_2: [..., ..., ...],
            inner_dic1_3: [..., ..., ...]},
     dic2: {inner_dic2_1: [..., ..., ...],
            inner_dic2_2: [..., ..., ...],
            inner_dic2_3: [..., ..., ...]},
     ... }
     to the flat list"""
    values_list = []
    for inner_dic in dic.values():
        for list_of_values in inner_dic.values():
            values_list += list_of_values
    return values_list

def merge_dics(dic1, dic2):
    """Put in one dict all unique keys with values. If there is two keys with
    the same names merge their values in one list without duplicates"""
    new_dic = deepcopy(dic1)
    for key in dic2:
        if key not in new_dic:
            new_dic[key] = dic2[key]
        else:
            new_dic[key] += dic2[key]
    # TODO next two lines looks like redundant. Check how many time it takes.
    for key in new_dic:
        new_dic[key] = del_duplicates_in_array(new_dic[key])
    return new_dic


def del_els_from_all_dic_values(els_to_remove, dic):
    new_dic = deepcopy(dic)
    if type(els_to_remove) is not list:
        exit('Wrong type was given in except keys!')
    for key in new_dic:
        for el in els_to_remove:
            if el in new_dic[key]:
                new_dic[key].remove(el)
    return new_dic

def del_els_from_all_dic_values_except_keys(els_to_remove, dic, except_keys):
    new_dic = deepcopy(dic)
    print type(els_to_remove)
    if type(except_keys) is str or type(except_keys) is int:
        new_dic.pop(except_keys)
    elif type(except_keys) is list or type(except_keys) is tuple:
        for key in except_keys:
            new_dic.pop(key)
    else:
        exit('Wrong type was given in except keys!')
    new_dic = del_els_from_all_dic_values(els_to_remove, new_dic)
    if type(except_keys) is str or type(except_keys) is int:
        new_dic[except_keys] = dic[except_keys]
    elif type(except_keys) is list or type(except_keys) is tuple:
        for key in except_keys:
            new_dic[key] = dic[key]
    return new_dic


def convert_dic_to_list_of_tuples(dict):
    """convert simple dic with list for values to the list of tuples:
    [(key1:value11),(key1:value12),...(key2:value12),(key2:value22),...)"""
    array = []
    for key in dict.keys():
        for el in dict.get(key):
            array.append((key,el))
    return array


# ARRAYS MANIPULATIONS
def shift_elements_in_array(array, crucial_el_index):
    new_array = []
    for index in xrange(len(array)):
        new_array.append(array[(index + crucial_el_index) % len(array)])
    return new_array

def del_duplicates_in_array(array):
    array_without_duplicates = []
    for el in array:
        if el not in array_without_duplicates:
            array_without_duplicates.append(el)
    return array_without_duplicates


def flatten_list(*lists):
    """convert [[list1],[list2]] into [list1, list2]"""
    return list(chain(*lists))


def zip_arrays_with_condition(condition, *arrays):
    result_array = filter(condition, zip(*arrays))
    return result_array


def create_array_with_condition(condition, array):
    result_array = filter(condition, array)
    return result_array


def array_diff(array1, array2):
    """array1=AB, array2=BC, you need to find A. AB / BC = A
    array1=A, array2=AB, you'll find None. So the order of arrays IS important"""
    result_array = []
    for el in array1:
        if el not in array2:
            result_array.append(el)
    return result_array

def array_symmetric_diff(array1, array2):
    # not used yet
    """array1=AB, array2=BC, you need to find AC. AB ∆ BC = AC
    The order of arrays IS NOT important"""
    result_array = []
    for el in array1:
        if el not in array2:
            result_array.append(el)
    for el in array2:
        if el not in array1:
            result_array.append(el)
    return result_array

def arrays_intersection(array1, array2):
    # not used yet
    """find which elements in two arrays are common"""
    result_array = []
    for el in array1:
        if el in array2:
            result_array.append(el)
    return result_array


def update_array(init_array, new_array):
    result_array = list(init_array)
    for el in new_array:
        if el not in init_array:
            result_array.append(el)
    return result_array


def convert_list_of_tuples_to_string_list(list_of_tuples):
    """convert list of tuples [(a1, b1). (a2, b2)] to the text:
    a1: b1
    a2: b2
    ...."""
    text = ''
    for el in list_of_tuples:
        text += str(el[0] + ' : ' + el[1] + '\n')
    return text

# TIME

def cur_t():
    return datetime.datetime.now().strftime('%H.%M.%S')


def cur_t_pure():
    return datetime.datetime.now()


def today_and_cur_t():
    return str(date.today().strftime('%Y.%m.%d')) + '_' + cur_t()


def today():
    return str(date.today().strftime('%Y.%m.%d'))


def count_down_sleep(timeout):
    """Sleep for time defined in timeout argument, and """
    counter = timeout
    while counter > 0:
        time.sleep(1)
        print 'waiting for... ', counter
        counter -= 1

#
def get_hrefs_from(array):
    """Get hrefs from list of elements and put it to the list"""
    result_array = []
    for el in array:
        result_array.append(el.get_attribute('href'))
    return result_array


def string_to_num(unicode_string):
    string  = unicode_string.encode()
    if string.find(',') != -1:
        string = string.replace(',', '')
    float_number = int(string)
    return float_number


# LOGGING

def debug_log(string):
    log = open('debug', 'a')
    log.write(string + "\n")
    log.close()


def debug_log_json(string):
    debug_log(json.dumps(string))


def do_nothing(*args):
    pass


# ASK

def ask(first_line, second_line, object, string=True):
    # TODO наверное не стоит принимать значения в int, т.к. они могут конвертироваться
    # TODO в нужную им сторону. Лучше все как string и это уже форматировать в числа
    def define_new_obj(second_line):
        try:
            new_obj = inp(second_line + '\n')
            return new_obj
        except NameError:
            define_new_obj('Wrong format! Please try again.\n' + second_line)

    result_obj = object
    if string:
        inp = raw_input
    else:
        inp = input
    while True:
        answer = raw_input((first_line + '\nY/N ?\n') % result_obj)
        if answer in ('Y', 'y', 'yes'):
            break
        elif answer in ('N', 'n', 'no'):
            result_obj = define_new_obj(second_line)
        else:
            print 'Sorry your answer \'%s\' is incorrect' % answer
    return result_obj




