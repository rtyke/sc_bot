db_username = ''

victim_labels = ["projectmooncircle", "platform", "factmag", "monkeytownrecords", "mixmag-1", "four-tet", "textrecords", "exitrecords", "flyinglotus", "ghostly"]

# label candiates:
# 'https://soundcloud.com/silent-disco-usa'
# 'https://soundcloud.com/blackest-ever-black'
# 'https://soundcloud.com/rarenoiserecords'
# 'https://soundcloud.com/hubro'
# 'https://soundcloud.com/monkeytownrecords'
# 'https://soundcloud.com/berlincommunityradio'

authorizeData = {'sc_username'  : {'mail': 'mail',
                               'password': 'password'},
                 'second_sc_username'  : {'mail' : 'mail',
                                'password' : 'password'}
                 }
# define mode. Test_mode = True: no login, no clicking
test_mode = False  # don't forget to create 'log_username_test' folder!
captcha = False
check_alert = True

account = 'sc_username'

# name of file where all data will be storaged
json_name = 'data'

# name of main logging file
logging_file = 'my_log'

# how many new friends we are going to follow
how_many_new_friends = 4

# how many unmutual users to unfollow
unfollow_x_users = 199

# how many users  to unfollow (unmutual and mutual)
unfolow_x_users_dc = 210

# how many likes to del
del_x_likes_dc = 210

update_inf_before_follow = False
update_inf_before_unfollow = False

# how many users one after another meaning that there is now new users on current label
freshman_x = 5

# how many futile labels one after another mean that label capacity is not enough
label_capacity_x = 3

# minimum user have to have X followers:
min_followers = 5

# maximum user have to have X followers:
max_followers = 5000

# max attempts to find element on a page
find_el_attempts = 1

# we will play track from A to B seconds
a = 1
b = 4

# spammer check
# we don't follow users who follow more than X users
# or follow less then Y users and followed by more than Z users (X, Y, Z)
spammer_check = (1500, 50, 1000)

dic_keys = ['blank', 'spammer', 'followers', 'following', 'liked_by_me',
            'ex_followers', 'ex_following' , 'deleted_likes','deleted_users',
            'deleted_tracks', 'liked_by_me_track_href']

