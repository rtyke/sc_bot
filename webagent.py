# -*- coding: utf-8 -*-
import selenium
from selenium import webdriver
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.action_chains import ActionChains
import time
import util
import sys
import essential_data as ed

class WebAgent:
    def __init__(self, captcha, logger=None, timeout=3, test_mode=True, check_alert=True):
        self.browser       = webdriver.Firefox()
        self.browser.set_page_load_timeout(30)
        # self.browser.implicitly_wait(10)  # TODO убрать отовсюду
        self.captcha       = captcha
        if logger is None: self.logger = util.do_nothing  # для возмжоности невызова модуля логирования
        else: self.logger  = logger.c_print
        self.test_mode     = test_mode
        self.timeout       = timeout
        self.check_alert   = check_alert
        self.cookies_text  = 'SoundCloud uses cookies. By using our services, '\
                             'you\'re agreeing to our Cookie Policy. We have ' \
                             'updated our Privacy Policy, effective as of 21 ' \
                             'August 2014. By using our services you\'re ' \
                             'agreeing to the updated policy.'

        self.xpaths = {
            'all_elements_on_a_page'    : "//body[1]",
            'announcement'              : "//div[starts-with(@class, "
                                          "'announcement')]",
            'loader'                    : "//div[@class = 'loading regular']",
            # 'blank_user'                : "//h1[@class ='sc-type-h2 sc-text']",
            'blank_user'                : "//div[@class ='emptyNetworkPage__image']",
            # login
            'loginform_email'           : "//input[@id='username']",
            'loginform_password'        : "//input[@id='password']",
            'loginform_authorize'       : "//input[@id='authorize']",
            'captcha'                   : "//div[@id='recaptcha_image']",
            'captcha_field'             : "//input[@id='recaptcha_response_field']",
            # new_panel "https://soundcloud.com/you/..." instead of "https://soundcloud.com/username/"
            'users_badge_np'            :
                "//a[starts-with(@class, 'genericBadge__heading')]",  # 36 users on page
            'user_name_in_likes_page'   :
                "//a[contains(@class, 'genericBadge__usernameHeading')]",
            # end of new panel
            'users_badge'               :
                "//a[@class='userAvatarBadge__usernameLink']",  # 25 users on page
            'artist_name_in_pls'        :
                "//div[@class = 'sound__header']//a[starts-with"
                "(@class, 'soundTitle__user')]",
            'user_name_in_comments'     :
                "//a [starts-with(@class, 'commentItem__username')]",
            'tracks_in_pls'             : "//div[@class = 'sound__header']",
            'track_name_in_pls'         :
                "//div[@class = 'sound__header']//a[starts-with("
                "@class, 'soundTitle__title sc-link-dark')]",
            # buttons
            'follow_button'             :
                # "//div[starts-with(@class, 'sc-button-toolbar')]"
                # "/button[starts-with(@ class, 'sc-button-follow')]",
                "//button[starts-with(@class, 'sc-button-follow sc-button sc-button-medium "
                "sc-button-responsive')]",
            'follow_button_on_user_badge':
                "//li[@class = 'usersList__item']/div[starts-with(@class , 'g-avatar-badge')]//button[starts-with(@class, 'sc-button-follow sc-button sc-button-small')]",
            'play_button'               :
                "//li//div[@class = 'sound__header']//button[@title = 'Play']",
            'like_button'               :
                "//li//button[starts-with(@class, 'sc-button-like')]",
            'signin_facebook_button'    :
                "//button[starts-with(@title, 'Sign')]",
            'sign_in_button'            :
                # "//a[starts-with(@class,'header__login')]",
                # "//button[@class = 'g-opacity-transition sc-button "
                # "sc-button-medium loginButton']",
                "//div[@class = 'frontHero__cta']/button[@title = 'Sign in']",
            'text_on_avatar'            :
                "//li[@class = 'usersList__item']//"
                "div[@aria-label = 'User stats']",
            'number_following_on_avatar':
                "//span[starts-with(@ class, 'sc-ministats')]/span[2]",
            'number_of_following'       :
                "//article[contains(@class, 'followingsModule')]"
                "//span[@class = 'sidebarHeader__actualTitle']",
            'number_of_followers'       :
                "//a[@class = 'sc-ministats sc-ministats-large "
                "sc-ministats-followers']//span[2]",
            # errors
            'error_on_page'             : "//*[@class = 'errorPage']",
            'no_user'                :
                "//*[@class = 'errorPage__inner error_404']",
            'smth_went_wrong'        : "//div[@class = 'errorPage']//h1[@class = 'errorTitle']",
            'test'                   :
                "//li[@class = 'usersList__item']//div[@aria-label = 'User stats']",
            'no_comments_on_track'   :
                "//div[@class = 'emptyNetworkPage emptyComments']",
            'no_sets_for_track'      :
                "//div[@class ='inSetsList__empty']",
        }

    def check_smth_went_wrong(self):
        smth_went_wrong = False
        if self.get_text('smth_went_wrong', waiting_time=1) == 'Sorry! Something went wrong':
            smth_went_wrong = True
        return smth_went_wrong

    def check_existence(self, xpath_name):
        LGM = self.logger
        existence = False
        xpath_string = self.__get_xpath_string(xpath_name)
        if self.browser.find_elements_by_xpath(xpath_string):
            existence = True
        return existence

    def check_504(self):
        """Ошибка 504 возникает после метода wa.open_url.
        Встраивать check_504 в open_url не оптимально - увеличивается
        время работы скрипта. Лучше всего вызывать check_504 только если после
        open_url будет ошибка. Такое внедрение check_504 требует кропотливости.
        Поэтому пока check_504 не применяем."""
        error_504 = False
        if self.get_text('all_elements_on_a_page') == '504 - Gateway Timeout':
            error_504 = True
        return error_504

    def check_announcement(self, ignore_text):
        announcement = False
        if self.check_alert:
            if self.check_existence('announcement'):
                if self.get_text('announcement') != ignore_text:
                    announcement = True
        return announcement

    def abort(self, message):
        util.s_abort()
        sys.exit(message)

    def click_test(self, obj, mode):
        if mode:
            print 'click %s in testing mode' % obj
        else:
            obj.click()

    def click_obj(self, obj):
        if self.test_mode:  # if test_mode == True
            print 'click %s in testing mode' % obj
        else:
            obj.click()

    def click_follow(self):
        # TODO if there is too many iterations stop script (may be banned)
        button = self.get_element_by_xpath_name(xpath_name='follow_button',
                                                wait_for='clickable')
        self.click_obj(button)  # click follow
        self.sleep()
        if not self.test_mode:
            while not self.user_is_followed_by_me():
                button = self.get_element_by_xpath_name(xpath_name='follow_button',
                                                        wait_for='clickable')
                self.click_obj(button)  # click follow

    def click_unfollow(self, check=True):
        if self.test_mode:
            check = False

        unfollow_done = False
        if check:
            counter = 0
            while self.user_is_followed_by_me() and counter < 2:
                counter += 1
                button = self.get_element_by_xpath_name(xpath_name='follow_button',
                                                        wait_for='clickable')
                self.click_obj(button)  # click unfollow
                time.sleep(1)  #  костыль см problem 0001
                if not self.user_is_followed_by_me():
                    unfollow_done = True
        else:  # no click in test mode
            button = self.get_element_by_xpath_name(xpath_name='follow_button',
                                                    wait_for='clickable')
            self.click_obj(button)  # click unfollow
            self.sleep()
            unfollow_done = True
        return unfollow_done

    def click_unlike_ex(self, like_button):
        counter = 0
        unlike_done = False
        if self.test_mode:
            unlike_done = True
        like_button.click()
        self.sleep()
        # TODO counter < 2: зашить кол-во проверок в essential_data.py. При c < 2 будет сделанно 3 клика
        # TODO если проверка не нужна, то сделать counter < 0
        while like_button.text in ("Liked", "Liked Playlist") and counter < 2:
            like_button.click()
            self.sleep()
            counter += 1
        if like_button.text in ("Like", "Like Playlist"):
            unlike_done = True
            if counter > 0:
                print 'Needed %s attempts to unlike it' % str(counter + 1)
        return unlike_done

    def click_unlike(self, liked_track, last_original_likes, check=True):
        """Возвращает False если нажатие на кнопку было два раза, а лайк не удалился"""
        # TODO move to dude.py
        # TODO create click_like method
        if self.test_mode:
            check = False

        unlike_done = False
        if check:
            counter = 0
            while not unlike_done and counter < 2:
                counter += 1
                self.click_obj(liked_track)
                self.sleep()
                liked = [like for like in last_original_likes
                         if like.text in ("Liked", "Liked Playlist")]
                if len(liked) == 0:  # if there is not likes among last likes
                    unlike_done = True
        else:
            self.click_obj(liked_track)
            self.sleep()
            unlike_done = True
        return unlike_done

    def user_is_followed_by_me(self):
        """return True or False if it explicitly known if I follow this user or
        not. Return None if button wasn't find because of no_user error"""
        followed = None
        button =\
            self.get_element_by_xpath_name('follow_button')
        if button:  # if button was found, if it wasn't then button is None
            text = button.text
            if text in ("Follow", 'Follow Back'):
                followed = False
            elif text == 'Following':
                followed = True
            else:
                self.abort('unexpected text on following button: %s' % text)
        return followed


    def text_on_follow_button(self):
        """temporary method"""
        button =\
            self.get_element_by_xpath_name('follow_button')
        if button:
            text = button.text
        return text

    def __get_xpath_string(self, xpath_name):
        return self.xpaths[xpath_name]

    def open_url(self, url):
        # TODO decide what to do in different cases when page doesn't found
        url = url
        try:
            self.browser.get(url)
        except selenium.common.exceptions.TimeoutException:
            self.open_url(url)
        self.sleep()

    def scroll_page(self, xpath_name):
        """Scroll page single time, wait for 3 seconds, and check if there is new
        information on the page after scroll. Return False if there is no new
         information and True if there is"""
        before = self.get_elements_by_xpath_name(xpath_name)
        self.browser.execute_script(
            'window.scrollTo(0, document.body.scrollHeight);')
        time.sleep(self.timeout)
        after = self.get_elements_by_xpath_name(xpath_name)
        if before == after:
            result = False  # there is nothing to scroll
        else:
            result = True
        return result

    def scroll_page_to_the_end(self, xpath_name, url = None):
        """Do scrolls and return how much scrolls is necessary for full page load.
        If scroll is not possible (new data wasn't loaded after scroll, the scroll
        will be done, but the counter wouldn't be updated"""
        counter = 0
        while self.scroll_page(xpath_name):
            counter += 1
        return counter

    def scroll_x_times(self, x, xpath_name):
        """scroll X times. Return False if X is bigger then it's necessary for
        full information loading"""
        scrl_counter = x
        while scrl_counter != 0:
            scroll = self.scroll_page(xpath_name)
            scrl_counter -= 1
            if not scroll:
                break
        if scrl_counter > 0:
            self.logger('Only %s from %s possible' % (scrl_counter, x))
            result = False  # X was too big
        else:
            result = True
        return result

    def open_url_collect_hrefs(self, url, xpath_name):
        elements_xpath = self.__get_xpath_string(xpath_name)
        self.open_url(url)
        self.scroll_page_to_the_end(xpath_name)
        print 'scrolling finished'
        elements_array = self.browser.find_elements_by_xpath(elements_xpath)
        hrefs_array = []
        for el in elements_array:
            hrefs_array.append(el.get_attribute('href'))
        return hrefs_array

    def close_browser(self):
        self.browser.close()

    def login(self):
        mail = ed.authorizeData[ed.account]['mail']
        passw = ed.authorizeData[ed.account]['password']
        browser = self.browser
        self.open_url("https://soundcloud.com")
        parent_h = browser.current_window_handle
        sign_in = self.get_element_by_xpath_name(wait_for='clickable',
                                                 xpath_name='sign_in_button')
        sign_in.click()
        handles = browser.window_handles  # before the pop-up window closes
        handles.remove(parent_h)
        browser.switch_to.window(handles.pop())

        email = self.get_element_by_xpath_name('loginform_email')
        email.send_keys(mail)

        password = self.get_element_by_xpath_name('loginform_password')
        password.send_keys(passw)
        login_button = self.get_element_by_xpath_name("loginform_authorize")
        login_button.click()
        self.sleep(4, True)
        try:
            self.check_existence('captcha_field')
            print 'CAPTCHA'
            self.captcha = True
            password = self.get_element_by_xpath_name('loginform_password')
            password.send_keys(passw)
            captcha_field = self.get_element_by_xpath_name('captcha_field')
            captcha_field.send_keys('')
            util.s_captcha()
            self.sleep(15, True)  # ручной ввод капчи
        except:
            pass
        browser.switch_to.window(parent_h)

    def number_of_elements_on_a_page(self, xpath_name):
        xpath = self.__get_xpath_string(xpath_name)
        return len(self.browser.find_elements_by_xpath(xpath))

    def sleep(self, timeout=None, countdown=None):
        if not timeout:
            sleep_for = self.timeout
        else:
            sleep_for = timeout
        if not countdown:
            time.sleep(sleep_for)
        else:
            util.count_down_sleep(sleep_for)


    def get_current_url(self):
        return self.browser.current_url

    def get_text(self, xpath_name, waiting_time=30):
        # TODO implement in more functions
        return self.get_element_by_xpath_name(xpath_name, waiting_time=waiting_time).text

    def get_elements_by_xpath_name(self, xpath_name):
        xpath_string = self.__get_xpath_string(xpath_name)
        return self.browser.find_elements_by_xpath(xpath_string)

    # Функция с wait. 84600 секунд это 24 часа. Сделать:
    # задавать waiting_time в essential_data.py
    def get_element_by_xpath_name(self, xpath_name,
                                  try_counter=0,
                                  waiting_time=30,
                                  wait_for='presence'):
        """Will return element. Will return None if can't find element for
        waiting time and:
        - there is no_user_error OR
        - there is smth_went_wrong error AND page was reload several times OR
        - there is some error OR
        - some obscure reason"""
        assert wait_for in ['clickable', 'visibility', 'presence'], \
            'argument wait_for of get_element_by_xpath_name method have to be' \
            '\'clickable]\' or \'visibility\' or \'presence\''
        LGM = self.logger
        element = None
        if try_counter > 1:
            LGM('Too many attempts (% s) to get el %s' % (try_counter, xpath_name))
        else:
            xpath_string = self.__get_xpath_string(xpath_name)
            try:
                if wait_for == 'clickable':
                    element = self.__wait_for_clickable(waiting_time, xpath_string)
                elif wait_for == 'visability':
                    element = self.__wait_for_visibility(waiting_time, xpath_string)
                elif wait_for == 'presence':
                    element = self.__wait_for_presence(waiting_time, xpath_string)
            except selenium.common.exceptions.TimeoutException:
                LGM('cant find el (%s) for %s seconds yet!'
                    % (xpath_name, waiting_time))
                if self.check_existence('smth_went_wrong'):
                    LGM('Error: smth went wrong. Reload the page')
                    url = self.get_current_url()
                    self.open_url(url)
                    element = self.get_element_by_xpath_name(xpath_name,
                                                             try_counter + 1,
                                                             waiting_time,
                                                             wait_for)
        return element

    def __wait_for_visibility(self, waiting_time, xpath_string):
        return WebDriverWait(self.browser, waiting_time).until(
                EC.visibility_of_element_located((By.XPATH, xpath_string)))

    def __wait_for_clickable(self, waiting_time, xpath_string):
        return WebDriverWait(self.browser, waiting_time).until(
                EC.element_to_be_clickable((By.XPATH, xpath_string)))

    def __wait_for_presence(self, waiting_time, xpath_string):
        return WebDriverWait(self.browser, waiting_time).until(
                EC.presence_of_element_located((By.XPATH, xpath_string)))

    def update_href_array(self, array, xpath_name):
        """подходит только для массивов, которые обновляют хронолигически,
        т.е. все новые элементы массива ставятся на верх списка. Например ,
        подходит для follower и не подходит для following. В случае если, массив
        не обновляется хронологически, а новые элементы могут быть добавлены в
        середину или конец списка, надо собирать данные со всего массива."""
        xpath = self.__get_xpath_string(xpath_name)
        updated_array = list(array)
        len1, len2 = -1, len(updated_array)
        while len1 != len2:
            self.scroll_page()
            elements = self.browser.find_elements_by_xpath(xpath)
            for el in elements:
                updated_array.append(el.get_attribute('href'))
            updated_array = util.del_duplicates_in_array(updated_array)
            len1, len2 = len2, len(updated_array)
        return updated_array

    def get_visible_hrefs(self, xpath_name):
        elements_array = self.get_elements_by_xpath_name(xpath_name)
        hrefs_array = util.get_hrefs_from(elements_array)
        return hrefs_array

    def get_hrefs_from_page_in_array(self, url, xpath_name, scrl_to_the_end=True):
        self.open_url(url)
        if scrl_to_the_end:
            self.scroll_page_to_the_end(xpath_name)
        hrefs = self.get_visible_hrefs(xpath_name)
        return hrefs

    def get_hrefs_from_elements(self, elements):
        all_elements = self.get_elements_by_xpath_name(elements)
        hrefs = util.get_hrefs_from(all_elements)
        return hrefs

    def get_href_from_el(self, element_xpath, waiting_time=30):
        return self.get_element_by_xpath_name(element_xpath, waiting_time=waiting_time).get_attribute('href')

    def get_tracks_titles_href_by(self, element):
        # TODO засечь сколько времени занимает
        titles_hrefs = self.get_visible_hrefs('track_name_in_pls')
        likes = self.get_elements_by_xpath_name('like_button')
        tuples_list = zip(titles_hrefs, likes)
        fit_tuple = [x for x in tuples_list if x[1] == element][0]
        title_href = fit_tuple[0]
        return title_href

    def move_on_element(self, element):
        ActionChains(self.browser).move_to_element(element)





