# -*- coding: cp1251 -*-
import webagent
import datapersistence
import dude as dude_module
import util as u
import essential_data
import logger as logger_module

if __name__ == '__main__':
    # variables
    ed = essential_data
    log_folder = 'log_' + ed.account
    user_url = 'https://soundcloud.com/' + ed.account
    logger = logger_module.Logger(folder=log_folder,
                                  file_name=ed.logging_file)
    dp = datapersistence.DataPersistence(folder=log_folder,
                                         test_mode=ed.test_mode)
    wa = webagent.WebAgent(captcha=False,
                           logger=logger,
                           test_mode=ed.test_mode,
                           check_alert=ed.check_alert)
    dude = dude_module.Dude(webagent=wa,
                            essential_data=ed,
                            dp=dp,
                            logger=logger)

    LGM = logger.c_print
    dude.preparation()
    # go!
    data_dic = dp.open_json(template='data',
                            position='last')

    if ed.update_inf_before_unfollow:
        tracks_response_dic = dude.get_tracks_responses_for_user(user_url)
        if len(tracks_response_dic) == 0:
            wa.abort("Error via collecting data about track responses. No responses")
        followers = dude.get_followers(user_url)
        data_dic['followers'] = list(followers)
        ex_followers = u.array_diff(data_dic['followers'], followers)
        data_dic['ex_followers'] = u.update_array(data_dic['ex_followers'],
                                                  ex_followers)
        dp.serialize_array(array=tracks_response_dic,
                           file_name=u.today_and_cur_t() + '_tracks_response_dic')
    else:
        tracks_response_dic = {}

    skip_list = dude.skip_list_for_unfollow(tracks_response_dic, data_dic)

    wa.login()

    unfollow_unlike_dic = dude.unfollow_unlike(data_dic, skip_list, ed.unfollow_x_users)
    if unfollow_unlike_dic['critical_error']:
        LGM('!!!!!!ATTENTION PLEASE!!!!!!!!!!'
            '\nThere was critical error during this unfollowing, script was stopped'
            '\n(all collected data was storaged in jsons)'
            '\nCheck why it was aborted\n'
            '!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!')

    if unfollow_unlike_dic.get('sc_failure'):
        LGM('There was soundcloud failures during attempt to unlike or '
            'unfollow this users \n%s\n. Please check log to get detailed '
            'information' % unfollow_unlike_dic['sc_failure'])
        unfollow_unlike_dic.pop('sc_failure')


    # update dic
    ex_following = list(unfollow_unlike_dic['unfollowing'])
    deleted_likes = list(unfollow_unlike_dic['deleted_likes'])
    deleted_users = list(unfollow_unlike_dic['deleted_users'])
    deleted_tracks = list(unfollow_unlike_dic['deleted_tracks'])
    data_dic['following'] = u.array_diff(data_dic['following'],
                                         ex_following)
    data_dic['ex_following'] = u.update_array(data_dic['ex_following'],
                                              ex_following)
    data_dic['liked_by_me'] = u.array_diff(data_dic['liked_by_me'],
                                           deleted_likes)
    data_dic['deleted_likes'] = u.update_array(data_dic['deleted_likes'],
                                               deleted_likes)
    # data_dic['followers'] = list(followers)

    if deleted_tracks:
        data_dic['deleted_tracks'] = u.update_array(data_dic['deleted_tracks'],
                                                    deleted_tracks)
        data_dic['liked_by_me'] = u.array_diff(data_dic['liked_by_me'],
                                               deleted_tracks)
        # ���� ������ �� ������� ������ �� ������ liked_by_me_track_href
        # ��� ��� ����� �� ������ �������� �� ������ �������, ����� �����
        # ������ � � ��� ����� ��������� �������
        LGM('!!!!!!ATTENTION PLEASE!!!!!!!!!!'
            '\nThere was deleted likes during this run of script.'
            '\nThere it is: %s' % deleted_tracks)

    if deleted_users:
        data_dic = u.del_els_from_all_dic_values(els_to_remove=deleted_users,
                                                 dic=data_dic)
        data_dic['deleted_users'] = u.update_array(data_dic['deleted_users'],
                                                   deleted_users)

    # serialize dictionary
    dp.serialize_array(array=data_dic,
                       file_name=u.today_and_cur_t() + '_' + ed.json_name)

    if unfollow_unlike_dic['critical_error']:
        u.s_bad_finish()
    else:
        u.s_finish()











