# -*- coding: utf-8 -*-
import json
import util as u
from datetime import date
import os
import sys
import shutil

class DataPersistence:
    def __init__(self, folder, test_mode=False):
        self.today     = date.today().strftime('%Y.%m.%d')
        self.folder    = folder
        self.test_mode = test_mode

    # no usages yet
    def get_list_of_files(self, folder=None):
        if not folder:
            folder = self.folder
        files = os.listdir(folder)
        return files
    def get_name_of_last_file(self, template, file_ext):
        files = sorted([x for x in os.listdir(self.folder)
                        if x.endswith(template + '.' + file_ext)])
        last_file = files[-1]
        return last_file

    def if_file_or_folder_exists(self):
        exists = False
        if os.path.exists(self.folder):
            exists = True
        return exists

    def no_this_extension_in_folder(self, extension, folder):
        exist = False
        files_lst = self.get_list_of_files(folder)
        print files_lst
        print filter(lambda x: x[-len(extension):] == extension, files_lst)
        if filter(lambda x: x[-len(extension):] == extension, files_lst):
            exist = True
        return exist


    def check_folder_existence_create_in_need(self, folder):
        if not os.path.exists(folder):
            os.makedirs(folder)
            return False
        else:
            return True

    def open_file(self, file_name, file_ext):
        file_to_open = open(self.folder + '/' + file_name + '.' + file_ext, 'r')
        return file_to_open

    def open_last_file(self, template, file_ext):
        files = sorted([x for x in os.listdir(self.folder)
                        if x.endswith(template + '.' + file_ext)])
        last_file = files[-1]
        open_file = open(self.folder + '/' + last_file, 'r')
        return open_file

    def open_json(self, template, position):
        """return blank dictionary if there is no jsons"""
        files = sorted([x for x in os.listdir(self.folder)
                        if x.endswith(template + '.json')])
        if files == []:
            print 'No jsons in folder'
            return {}
        if position == 'last':
            pos_int = -1
        elif position == 'next_to_last':
            pos_int = -2
        else:
            sys.exit("Error in position of file in array")
        file_to_open = files[pos_int]
        print file_to_open
        file_to_open = open(self.folder + '/' + file_to_open, 'r')
        # print type(file_to_open), file_to_open
        json_file = json.loads(file_to_open.read())
        return json_file

    def open_certain_json(self, file_name):
        log = open(self.folder + '/' + file_name + '.json', 'r')
        data = json.loads(log.read())
        return data

    def serialize_array(self, array, file_name):
        """ """
        array_json = json.dumps(array)
        if os.path.exists(self.folder  + '/'):
            pass
        else:
            os.mkdir(self.folder + '/')
        log = open(self.folder  + '/' + file_name + '.json', 'w')
        log.write(array_json)

    def save_txt(self, text, file_name):
        """Save text in file to the folder from essential data"""
        if os.path.exists(self.folder  + '/'):
            pass
        else:
            os.mkdir(self.folder  + '/')
        log = open(self.folder  + '/' + file_name + '.txt', 'w')
        log.write(text)

    def create_backup(self):
        """Create backup of all files except the .mov files from from log folder
        in soundcloud/bu/date+time"""
        path = os.getcwd() + '/bu/'
        files = os.listdir(self.folder)
        files_data = [x for x in files if x[-3:] != 'mov']
        folder_name = u.today_and_cur_t()
        os.mkdir(path + folder_name)
        for file in files_data:
            source = os.getcwd() + self.folder + '/' + file
            destination = path + folder_name
            shutil.copy2(source, destination)
        return folder_name
