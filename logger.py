# -*- coding: utf-8 -*-
import os
import logging
import util

class Logger:
    def __init__(self, folder, file_name, debug_mode=True):
        self.folder     = folder
        self.file_name  = file_name
        self.debug_mode = debug_mode
        # create logger
        logger = logging.getLogger('Logger')
        if self.debug_mode:
            logger.setLevel(logging.DEBUG)
        if not os.path.exists(folder):
            os.makedirs(folder)
        # create console handler and set level
        ch = logging.StreamHandler()
        ch.setLevel(logging.DEBUG)
        # file handler
        fh = logging.FileHandler(str(self.folder + '/' + \
                                     util.today() + '_' + \
                                     self.file_name + '.log'))
        fh.setLevel(logging.DEBUG)
        # create formatter
        formatter = logging.Formatter('%(asctime)s      | %(message)s', '%H:%M:%S')
        formatter_file = logging.Formatter('%(asctime)s      | %(message)s')
        # add formatter to ch
        ch.setFormatter(formatter)
        fh.setFormatter(formatter_file)
        # add ch to logger
        logger.addHandler(ch)
        logger.addHandler(fh)
        self.logger = logger

    def c_print(self, text):
        return self.logger.debug(text)




